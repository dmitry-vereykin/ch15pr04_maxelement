/**
 * Created by Dmitry Vereykin on 7/28/2015.
 * Thanks to Thomas O'Shaughnessy for helping and explanation.
 */

public class MaxElement {
    public static void main(String[] args) {
        int[] testArray = {2, 35, 12, 64, 46, 5, 52};
        System.out.print(maxElement(testArray, 0));
    }

    public static int maxElement(int[] array, int pos) {
        int max;

        if (pos < array.length - 1) {
            max = maxElement(array, pos + 1);

            if (array[pos] > max)
                max = array[pos];
        } else {
            max = array[pos];
        }

        return max;
    }
}