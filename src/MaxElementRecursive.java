/**
 * Created by eXrump on 6/12/2016.
 */
public class MaxElementRecursive {
    public static void main(String[] args) {
        int[] testArray = {2, 102, 35, 12, 64, 46, 5, 52, 72};
        System.out.print(findMax(testArray, 8));
    }

    public static int findMax(int[] array, int n) {

        if (n == 0)
            return array[n];

        if (array[n] > array[n - 1])
            array[n - 1] = array[n];

        return findMax(array, n - 1);
    }
}
